# Comment model represents the Comment table
class Comment < ActiveRecord::Base
  attr_accessible :time_posted, :content, :post_id
  
  validates :time_posted, :content, :presence => true
  
  belongs_to :post
  belongs_to :user

  # Converts this element to JSON while including user data with no personal info
  def to_json_with_clean_user()
    self.to_json(:include => { :user => { :except => [ :email, :loc_lat, :loc_lon, :ident ] } })
  end

  # Converts this element to JSON while including user data with no personal info
  def as_json_with_clean_user()
    self.as_json(:include => { :user => { :except => [ :email, :loc_lat, :loc_lon, :ident ] } })
  end
  
  # Converts this element to JSON while including user data
  def to_json_with_user()
    self.to_json(:include => :user )
  end

  # Converts this element to JSON
  def to_json(options={})
    if options.length == 0
      return self.to_json_with_clean_user
    else
      return super.to_json(options)
    end
  end

  # Converts this element to JSON
  def as_json(options={})
    if options.length == 0
      return self.as_json_with_clean_user
    else
      return super.as_json(options)
    end
  end
end