# Tag model represents the Tag table
class Tag < ActiveRecord::Base
  attr_accessible :name
  
  validates :name, :presence => true
  has_many :posts
end