# Post model represents the Post table
class Post < ActiveRecord::Base
  attr_accessible :time_posted, :content, :loc_lat, :loc_lon, :radius, :tag_id
  
  validates :time_posted, :content, :loc_lat, :loc_lon, :radius, :presence => true
  
  belongs_to :user
  
  has_many :comments, :dependent => :destroy
  belongs_to :tag
  
  # Converts this element to JSON while including user data with no personal info
  def to_json_with_clean_user()
    self.to_json(:include => { :user => { :except => [ :email, :loc_lat, :loc_lon, :ident ] }, :tag => {} })
  end

  # Converts this element to JSON while including user data with no personal info
  def as_json_with_clean_user()
    self.as_json(:include => { :user => { :except => [ :email, :loc_lat, :loc_lon, :ident ] }, :tag => {} })
  end
  
  # Converts this element to JSON while including user data
  def to_json_with_user()
    self.to_json(:include => :user )
  end

  # Converts this element to JSON
  def to_json(options={})
    if options.length == 0
      return self.to_json_with_clean_user
    else
      return super.to_json(options)
    end
  end

  # Converts this element to JSON
  def as_json(options={})
    if options.length == 0
      return self.as_json_with_clean_user
    else
      return super.as_json(options)
    end
  end
  
  # Converts this element to a string
  def to_s()
    self.content + ' posted on ' + self.time_posted.to_s + ' by ' + self.user.display_name
  end
end