# User model represents the User table
class User < ActiveRecord::Base
  attr_accessible :ident, :photo, :date_registered, :email, :display_name, :loc_lat, :loc_lon
  
  validates :ident, :date_registered, :email, :presence => true
  validates_uniqueness_of :ident
  validates :display_name, :presence => true, :length => { :minimum => 3 }
  
  has_many :posts
  has_many :comments
  
  # Converts this element to JSON while removing personal data
  def to_json_with_clean_user()
    self.to_json(:except => [ :email, :loc_lat, :loc_lon, :ident ])
  end

  # Converts this element to JSON while removing personal data
  def as_json_with_clean_user()
    self.as_json(:except => [ :email, :loc_lat, :loc_lon, :ident ])
  end

  # Returns true if this is a new user
  def isNewUser()
    self.loc_lat == 999999 && self.loc_lon == 999999
  end

  # Converts this element to JSON
  def to_json(options={})
    if options.nil?
      self.to_json_with_clean_user
    else
      super.to_json(options)
    end
  end

  # Converts this element to JSON
  def as_json(options={})
    if options.nil?
      self.as_json_with_clean_user
    else
      super.as_json(options)
    end
  end
  
  # Converts this element to a string
  def to_s()
    self.display_name + ' - ' + self.email + ' [' + self.ident + ']'
  end
end