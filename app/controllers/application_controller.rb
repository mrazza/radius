# Base application controller provides shared functionality
class ApplicationController < ActionController::Base
  protect_from_forgery
  
  # Verifies the user is logged in
  def checkLogin
    # Is user logged in?
    if session[:user].nil?
      redirect_to :controller => 'home', :action => 'index' # If not redirect to landing page
      return false
    end
  end
  
  # Determines if the current user is a new user; if so redirect them to the "newUser" page
  def checkNewUser
    if !session[:user].nil?
      if session[:user].isNewUser
        redirect_to :controller => 'home', :action => 'newUser'
        return false
      end
    end
  end
end
