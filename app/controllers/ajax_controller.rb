# This class contains all AJAX request functions
class AjaxController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_filter :checkLogin # Require the user to be authenticated before we will process anything (see application controller)
  
  # Create's a post with the specified content, tag, and radius at the user's location
  # Required Post Fields:
  #   content -> The content of the post
  #   tag -> The tag for the post
  #   radius -> The radius of the post
  # Returns:
  #   JSON of the newly created post.
  def createPost
    # Verify the required POST fields exist
    if params[:tag].nil? || params[:content].nil? || params[:radius].nil?
      onBadParams
      return
    end
    
    user = session[:user] # Get the user
    tag = Tag.find_by_name(params[:tag]) # Check if the tag exists yet
    
    # If the tag does not yet exist, create it
    if tag.nil?
      tag = Tag.create(:name => params[:tag])
    end
    
    # Create the new post
    newPost = user.posts.create(:time_posted => Time.now(), :content => params[:content], :loc_lat => user.loc_lat, :loc_lon => user.loc_lon, :radius => params[:radius], :tag_id => tag.id)
      
    begin
      newPost.save! # Attempt to save it
    rescue
      render :text => 'Failure.', :status => 500 # Failed to save
      return
    end
    
    render :json => newPost.to_json # Saved, send back the new post ID
  end
  
  # Get's all the posts whose radius contains the user's location
  # No Required Post Fields
  # Returns:
  #   JSON array of all posts within the radius.
  def getPosts
    user = session[:user] # Get the user
    
    # Perform the query
    posts = Post.where('(loc_lat - radius <= :user_lat AND loc_lat + radius >= :user_lat) AND (loc_lon - radius <= :user_lon AND loc_lon + radius >= :user_lon)', {:user_lat => user.loc_lat, :user_lon => user.loc_lon})
  
    render :json => posts.to_json # Send back the data as JSON (exclude personal user data)
  end
  
  # Get's all the new posts whose radius contains the user's location
  # Required Fields:
  #   lastUpdateTime -> The time since the last update
  # Returns:
  #   JSON array of all new posts within the radius.
  def getNewPosts
    # Verify the required POST fields exist
    if params[:lastUpdateTime].nil?
      onBadParams
      return
    end
    
    user = session[:user] # Get the user
    lastUpdateTime = params[:lastUpdateTime]
    
    # Perform the query
    posts = Post.order(:time_posted).where('(loc_lat - radius <= :user_lat AND loc_lat + radius >= :user_lat) AND (loc_lon - radius <= :user_lon AND loc_lon + radius >= :user_lon) AND time_posted >= :lastUpdateTime', {:user_lat => user.loc_lat, :user_lon => user.loc_lon, :lastUpdateTime => lastUpdateTime})
  
    render :json => posts.to_json
  end
  
  # Get's all of a specific user's non-personal data
  # Optional Fields:
  #   userID: If no user ID is specified, returns the data for the current logged in user
  # Returns:
  #   JSON data for the specified user or current logged in user
  def getUserData
    # Determine if a user id is specified
    if params[:userID].nil?
       render :json => session[:user].to_json
       return
    end
    
    requestedUser = User.find(params[:userID])
    
    if requestedUser.nil?
      onBadParams
      return
    end
    
    render :json => requestedUser.to_json
  end
  
  # Gets all the comments associated with a particular post.
  # Required Fields:
  #   postID: ID of the post to get the comments of
  # Returns:
  #   JSON array of all the comments (and user data) associated with the post
  def getComments
    # Determine if the required arguments exist
    if params[:postID].nil?
      onBadParams
      return
    end

    thePost = Post.find(params[:postID])

    if (thePost.nil?)
      onBadParams
      return
    end

    render :json => thePost.comments.to_json
  end

  #createComment
  # Creates a comment on the specified post.
  # Required Fields:
  #   postID: ID of the post we're commenting on
  #   content: The content of the comment
  # Returns:
  #   JSON of the added comment
  def createComment
    if params[:postID].nil? || params[:content].nil?
      onBadParams
      return
    end

    thePost = Post.find(params[:postID])

    if thePost.nil?
      onBadParams
      return
    end

    theComment = session[:user].comments.create(:post_id => thePost.id, :content => params[:content], :time_posted => Time.now)

    begin
      theComment.save!
    rescue
      render :text => 'Failure', :status => 500
      return
    end

    render :json => theComment.to_json
  end
  #removeComment

  # Removes the specified post from the database, this post must belong to the current logged in user
  # Required Fields:
  #   postID: ID of the post to remove
  # Returns:
  #   Status 200 if okay; otherwise status 500
  def removePost
    # Determine if the required arguments exist
    if params[:postID].nil?
      onBadParams
      return
    end

    thePost = Post.find(params[:postID])

    if thePost.nil? || thePost.user.id != session[:user].id
      onBadParams
      return
    end

    begin
      thePost.destroy
    rescue
      render :text => "Failure!", :status => 500
      return
    end

    render :nothing => true, :status => 200
  end
  
  # This is an internal method that is used when reporting bad parameters in an AJAX request
  def onBadParams
    render :text => 'Missing/Invalid Params', :status => 500
  end
end
 