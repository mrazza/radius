require 'rpx_helper'

# Main controller for user-facing website
class HomeController < ApplicationController
  include RpxHelper
  before_filter :checkLogin, :only => :main
  before_filter :checkNewUser, :only => :main
  
  # Landing page
  def index
    if !session[:user].nil?
      redirect_to :action => 'main' # If user is logged in take them to the main page
    end
  end
  
  # This performs RPX login integration and interfaces with the RPX login client
  def login
    rpxInstance = RpxHelper.new("2f7277d13f3f7cfc456fb167f9dadf9885a7fcd4", "https://rpxnow.com", "")
    info = rpxInstance.auth_info(params[:token])
    
    user = User.find_by_ident(info['identifier'])
    
    newUser = user.nil?
    
    if newUser
      user = User.new(:ident => info['identifier'], :photo => info['photo'], :date_registered => DateTime.now, :email => info['email'], :display_name => info['displayName'], :loc_lat => 999999, :loc_lon => 999999)
      user.save!
    end
    
    reset_session
    session[:user] = user
    
    if user.isNewUser
      redirect_to :action => "newUser" # Redirect to the new user page
    else
      redirect_to :action => "main" # Login complete, redirect to main page
    end
  end
  
  # Prompts the new user to enter address information
  def newUser
    if !params[:address].nil?
      user = session[:user]
      location = Gmaps4rails.geocode(params[:address])
      user.loc_lat = location[0][:lat]
      user.loc_lon = location[0][:lng]
      user.save!
      redirect_to :action => "main"
      
      return
    end
    
    render :locals => { :user => session[:user] }
  end
  
  # List the current users
  def listUsers
    render :locals => { :users => User.all }
  end
  
  # Log out
  def logout
    session[:user] = nil
    
    redirect_to :action => "index"
  end
  
  # Main user page
  def main
    render :locals => { :user => session[:user] }
  end
end
