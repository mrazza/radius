<?php
	session_start();
	$mapstring = '';
	$boxInfo = '';
	$listener = '';
	$mapstring2 = '';
	$boxInfo2 = '';
	$listener2 = '';
	$dbname = $user = $pass = 'rpinav';
	//$dbname = $user = 'jgwitkow_rpinav';
	//$pass = 'rpinav';
	// Connect to the database
	try {

  $dbconn = new PDO('mysql:host=localhost;dbname='.$dbname, $user, $pass);
	}
	catch (Exception $e) {
		echo "Error: " . $e->getMessage();
	}
  
  $stmt = $dbconn->query("SELECT * FROM places");
  $stmt->execute();

  $markerArray;
  $countInfo = 0;
foreach ($stmt as $row) {
  $countInfo++;
  $category = $row['category'];
  if ($category == "academic"){
    $icon = "academic2.png";
  }
  if ($category == "athletic"){
    $icon = "sports.png";
  }
  if ($category == "shuttle"){
    $icon = "shuttle.png";
  }
  if ($category == "frat"){
    $icon = "ifc.png";
  }
  if ($category == "fraternity"){
    $icon = "panhel.PNG";
  }
  if ($category == "food"){
    $icon = "food.png";
  }
  if ($category == "misc"){
    $icon = "pin.png";
  }
  if ($category == "sorority"){
    $icon = "panhel.PNG";
  }
  if ($category == "dorm"){
    $icon = "res_Icon.png";;
  }  
  $name = $row['name'];
  $varname = "loc" . $row['pid'];
  $newplace = "var ". $varname ."loc = new google.maps.LatLng(".$row['lon'].",".$row['lat']."); var ". $varname ."= new google.maps.Marker({ visible: false, position: ". $varname ."loc, map: map, icon: '".$icon."' }); " . $varname . ".mycategory ='" . $category ."'; markersArray.push(" . $varname .");" ;
  $pathname = "review.php?pid=".$row['pid'];
  
  $mapstring = $mapstring . $newplace; 
  if($category == "food"){
    if(isset($_SESSION['admin'])){
       $newContent = "var content" . $countInfo . " = \"<div id='info'><h1 id='title'>" . $name . "</h1><p id='content'><img id='pic' height = '150px' width='200px' src='" . $row['photo'] . "' />" . $row['description'] . "</p><a href='".$pathname."'>Reviews</a><form action='termproject.php' method='post' /><input type='submit' value='Delete' id='dbutton' name='delete'/><input type='hidden' name='pid' value='".$row['pid']."'/></form><form action='termproject.php' method='get' /><input type='submit' value='Edit' id='ebutton' name='edit'/><input type='hidden' name='pid' value='".$row['pid']."'/></form></div>\";";
      }
    else{
      $newContent = "var content" . $countInfo . " = \"<div id='info'><h1 id='title'>" . $name . "</h1><p id='content'><img id='pic' height = '150px' width='200px' src='".$row['photo']."' />" . $row['description'] . "</p><a href='".$pathname."'>Reviews</a></div>\";";
    }
  }
  
  else{
    if(isset($_SESSION['admin'])){
       $newContent = "var content" . $countInfo . " = \"<div id='info'><h1 id='title'>" . $name . "</h1><p id='content'><img id='pic' height = '150px' width='200px' src='" . $row['photo'] . "' />" . $row['description'] . "</p><form action='termproject.php' method='post' /><input type='submit' value='Delete' id='dbutton' name='delete'/><input type='hidden' name='pid' value='"  .$row['pid']. "'/></form><form action='termproject.php' method='get' /><input type='submit' value='Edit' id='ebutton' name='edit'/><input type='hidden' name='pid' value='".$row['pid']."'/></form></div>\";";
    }
    else{
      $newContent = "var content" . $countInfo . " = \"<div id='info'><h1 id='title'>" . $name . "</h1><p id='content'><img id='pic' height = '150px' width='200px' src='" . $row['photo'] . "' />" . $row['description'] . "</p></div>\";";
    }
  }
  $newlistener = " google.maps.event.addListener(". $varname .", 'click', function() { infowindow" . $countInfo . ".open(map," . $varname . ");});";
  
 

   $infoWindow = "var infowindow" . $countInfo . " = new google.maps.InfoWindow({ content: content" . $countInfo . "});";
  $boxInfo = $boxInfo  ;
  $listener = $listener  . $newContent . $infoWindow . $newlistener;
}

if(isset($_POST['delete']) && $_POST['delete'] == 'Delete'){
  $stmt = $dbconn->prepare("DELETE FROM places WHERE pid =  :pid ");
  $stmt->execute(array(':pid' => $_POST['pid'] ));
}

if(isset($_GET['edit']) && $_GET['edit'] == 'Edit'){
  $loc = 'admin.php?id='.$_GET['pid'];
  header('Location :'.$loc);
  
}
$countInfo2 = 0;
if (isset($_POST['search'])){
    $sql = "SELECT * FROM places WHERE name LIKE :search";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array(':search' => '%'.$_POST['search'].'%'));
    
    foreach ($stmt as $row) {
      $countInfo2++;
      
      $category = $row['category'];
  if ($category == "academic"){
    $icon = "academic2.png";
  }
  if ($category == "athletic"){
    $icon = "sports.png";
  }
  if ($category == "shuttle"){
    $icon = "shuttle.png";
  }
  if ($category == "frat"){
    $icon = "ifc.png";
  }
  if ($category == "fraternity"){
    $icon = "panhel.PNG";
  }
  if ($category == "food"){
    $icon = "food.png";
  }
  if ($category == "misc"){
    $icon = "pin.png";
  }
  if ($category == "sorority"){
    $icon = "panhel.PNG";
  }
  if ($category == "dorm"){
    $icon = "res_Icon.png";
  }    
$name = $row['name'];
  $varname2 = "locsearch" . $row['pid'];
  $newplace2 = "var ". $varname2 ."loc = new google.maps.LatLng(".$row['lon'].",".$row['lat']."); var ". $varname2 ."= new google.maps.Marker({ visible: true, position: ". $varname2 ."loc, map: map, icon: '".$icon."' }); " . $varname2 . ".mycategory ='" . $category ."'; markersArray.push(" . $varname2 .");" ;
  $pathname = "review.php?pid=".$row['pid'];
  
  
  $mapstring2 = $mapstring2 . $newplace2;
  
    if($category == "food"){
    if(isset($_SESSION['admin'])){
       $newContent2 = "var content" . $countInfo2 . " = \"<div id='info'><h1 id='title'>" . $name . "</h1><p id='content'><img id='pic' height = '150px' width='200px' src='" . $row['photo'] . "' />" . $row['description'] . "</p><a href='".$pathname."'>Reviews</a><form action='termproject.php' method='post' /><input type='submit' value='Delete' id='dbutton' name='delete'/><input type='hidden' name='pid' value='".$row['pid']."'/></form><form action='termproject.php' method='get' /><input type='submit' value='Edit' id='ebutton' name='edit'/><input type='hidden' name='pid' value='".$row['pid']."'/></form></div>\";"; 
    }
    else{
      $newContent2 = "var content" . $countInfo2 . " = \"<div id='info'><h1 id='title'>" . $name . "</h1><p id='content'><img id='pic' height = '150px' width='200px' src='".$row['photo']."' />" . $row['description'] . "</p><a href='".$pathname."'>Reviews</a></div>\";";
    }
}
  
  else{
    if(isset($_SESSION['admin'])){
       $newContent2 = "var content" . $countInfo2 . " = \"<div id='info'><h1 id='title'>" . $name . "</h1><p id='content'><img id='pic' height = '150px' width='200px' src='" . $row['photo'] . "' />" . $row['description'] . "</p><form action='termproject.php' method='post' /><input type='submit' value='Delete' id='dbutton' name='delete'/><input type='hidden' name='pid' value='"  .$row['pid']. "'/></form><form action='termproject.php' method='get' /><input type='submit' value='Edit' id='ebutton' name='edit'/><input type='hidden' name='pid' value='".$row['pid']."'/></form></div>\";";
    }
    else{
      $newContent2 = "var content" . $countInfo2 . " = \"<div id='info'><h1 id='title'>" . $name . "</h1><p id='content'><img id='pic' height = '150px' width='200px' src='" . $row['photo'] . "' />" . $row['description'] . "</p></div>\";";
    }
  }

  $newlistener2 = " google.maps.event.addListener(". $varname2 .", 'click', function() { searchinfowindow" . $countInfo2 . ".open(map," . $varname2 . ");});";
  //echo $newlistener2;
 

   $infoWindow2 = "var searchinfowindow" . $countInfo2 . " = new google.maps.InfoWindow({ content: content" . $countInfo2 . "}); searchinfowindow" .$countInfo2 .  ".open(map, " . $varname2 .");";
  $boxInfo2 = $boxInfo2  ;
  $listener2 = $listener2  . $newContent2 . $infoWindow2 . $newlistener2;       
}
}

?>
<!DOCTYPE html>
<html>
  <head>
    <title>RPINav</title>
    <!--<script type="text/javascript" src="jquery-1.7.1.js"></script>-->
    <link rel="stylesheet" type="text/css" href="termproject.css" />
 </head>
  <body>
    <div id="topbar">
      <a href="termproject.php" ><img id="logo" src="rpinav_logo.png" alt="logo" /></a>
			<?php if(isset($_SESSION['username'])) { ?>
				<form action="rcs.php" method="post">
					<input type="submit" id="login" value="Logout" name="logout" />
				</form>
				<div id="user">Welcome <?php echo $_SESSION['username']?></div>
			<?php } ?>
			<?php if(!isset($_SESSION['username'])) { ?>
				<form action="rcs.php" method="post">
					<input type="submit" id="login" value="Login" name="login"/>
				</form>
			<?php } ?>
			<div id="admin"><a href="admin.php">Admin Page</a></div>
    </div>
    <div id="sidebar">
      <div id="border">
        <div id="content">
          <div id="search">
            <h2>SEARCH</h2>
            <form action="termproject.php" method="POST">
              <input type="text" id ="search" name="search" /><br />
              <input type="submit" id="submitButton" name="searchsubmit" value="Submit" />
            </form>
          </div>
          <div id="categories">
            <h2>CATEGORIES</h2>
            <form> <!--action="termproject.php"-->
              <input id="academic" type="checkbox" checked="checked" name="academic" /> Academic Buildings<br /><!--value="$_POST['academic']"-->
              <input id="athletic" type="checkbox" value="$_POST['athletic']" name="athletic" /> Athletic Buildings<br />
              <input id="food" type="checkbox" checked="checked" value="$_POST['food']" name="food" /> Dining Halls<br />
              <input id="frat" type="checkbox" value="$_POST['frat']" name="frat" /> Fraternity Houses<br />
              <input id="misc" type="checkbox" value="$_POST['misc']" name="misc" /> Miscellaneous<br />
              <input id="parking" type="checkbox" value="$_POST['parking']" name="parking" /> Parking<br />
              <input id="dorm" type="checkbox" value="$_POST['dorm']" name="dorm" /> Residence Halls<br />
              <input id="sorority" type="checkbox" value="$_POST['sorority']" name="sorority" /> Sorority Houses<br />
              <input id="shuttle" type="checkbox" value="$_POST['shuttle']" name="shuttle" /> Shuttle Stops<br />
              <input type="button" id="filterbutton" name="filter" value="Filter" />
            </form>
          </div>
        </div>
      </div>
    </div>
    <div id="map"></div>

  <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?sensor=false">
    </script>
   
      <script type="text/javascript">
      var markersArray = [];
      
      function initialize() {
        
        //OPTIONS FOR MAP
        var myOptions = {
          //centers map at rpi in Troy, NY
          center: new google.maps.LatLng(42.73050, -73.67696),
          zoom: 17,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        
        //INITIALIZE MAP
        var map = new google.maps.Map(document.getElementById("map"),
            myOptions);
            map.setTilt(45);
            
        //MAP COLOR SETTINGS   
        var pinkParksStyles = [
  {
    featureType: "all",
    stylers: [
      { saturation: -80 }
    ]
  },
  {
    featureType: "landscape",
    stylers: [
      { hue: "#ff0023" },
      { saturation: 60 }
    ]
  }
];

map.setOptions({styles: pinkParksStyles}); 

 //INFO WINDOWS FOR EVENTLISTENERS
 
var contentString = '<div id="content">';

<?php echo $mapstring . $listener; 
      echo $mapstring2 . $listener2;
?>
//JEC BUILDING MARKER
  var jecloc = new google.maps.LatLng(42.730575,-73.681583);
  var jec = new google.maps.Marker({
      position: jecloc,
      map: map,
      icon: 'camera.png'
  });
  
  google.maps.event.addListener(jec, 'click', function() {
  //infowindow.open(map,jec);
  //alert('hello');
  
  panorama.setVisible(true);
});


}
//END OF INITIALIZE


//CALL INITIALIZE
      initialize(); 
      
//filter Test
  var filtered = [];
  var unfiltered = [];
  var academic = document.getElementById("academic"),
      athletic = document.getElementById("athletic"),
      food = document.getElementById("food"),
      frat = document.getElementById("frat"),
      parking = document.getElementById("parking"),
      dorm = document.getElementById("dorm"),
      misc = document.getElementById("misc"),
      shuttle = document.getElementById("shuttle"),
      sorority = document.getElementById("sorority");
      

//FILTER MARKERS      
document.getElementById('filterbutton').addEventListener('click', function checkFilter(){   
if(academic.checked) {
    filtered.push(academic.name);
  }
else
    unfiltered.push(academic.name);
if(athletic.checked) {
    filtered.push(athletic.name);
  }
else
    unfiltered.push(athletic.name);
if(food.checked) {
    filtered.push(food.name);
  }
else
    unfiltered.push(food.name);
if(frat.checked) {
    filtered.push(frat.name);
  }
else
    unfiltered.push(frat.name);
if(parking.checked) {
    filtered.push(parking.name);
  }
else
    unfiltered.push(parking.name);
if(misc.checked) {
    filtered.push(misc.name);
  }
else
    unfiltered.push(misc.name);
if(dorm.checked) {
    filtered.push(dorm.name);
  }
else
    unfiltered.push(dorm.name);
if(sorority.checked) {
    filtered.push(sorority.name);
  }
else
    unfiltered.push(sorority.name);
if(shuttle.checked) {
    filtered.push(shuttle.name);
  }
else
    unfiltered.push(shuttle.name);
if (filtered.length != 0){
for(var i = 0; i < filtered.length; i++){
  for (var j = 0; j < markersArray.length; j++)
    if (filtered[i] == markersArray[j].mycategory){
      markersArray[j].setVisible(true);
    }
  }
}
if (unfiltered.length != 0){
for(var i = 0; i < unfiltered.length; i++){
  for (var j = 0; j < markersArray.length; j++)
    if (unfiltered[i] == markersArray[j].mycategory){
      markersArray[j].setVisible(false);
    }
  }
} 
unfiltered = [];
filtered = [];

});
//SET INITIAL ICONS (ACADEMIC AND DINING) ONLY IF SEARCH IS NOT BEING USED
<?php if (!isset($_POST['search'])){ echo "document.getElementById('filterbutton').click();" ;}?>


//PANORAMA ATTEMPT
  
var panorama; var troyJec = new google.maps.LatLng(42.729625,-73.680483);
function initialize2() {
  var panoOptions = {
    pano: 'reception',
    visible: false,
    panoProvider: getCustomPanorama,
    linksControl: true,
    zoom: 0
  }
  panorama = new google.maps.StreetViewPanorama(
      document.getElementById('map'),panoOptions);
}

// Return a pano image given the panoID.
function getCustomPanoramaTileUrl(pano,zoom,tileX,tileY) {
  return 'panoLallySage.jpg';
}
function getCustomPanorama(pano,zoom,tileX,tileY) {

   switch(pano) {
    case 'reception':
      return {
        location: {
          pano: 'reception',
          description: "Troy NY - RPI",
          latLng: troyJec
        },
        copyright: 'RPI Navigator',
        tiles: {
          tileSize: new google.maps.Size(1024, 512),
          worldSize: new google.maps.Size(1024, 512),
          centerHeading: 105,
          getTileUrl: getCustomPanoramaTileUrl
        }
      };
      break;
  }
}
 initialize2();
//PINK?RED STYLING SET
map.setOptions({styles: pinkParksStyles}); 

 //INFO WINDOWS FOR EVENTLISTENERS
 
var contentString = '<div id="content">';
var infowindow = new google.maps.InfoWindow({
    content: contentString
});
 
google.maps.event.addListener(jec, 'click', function() {
  infowindow.open(map,jec);
  panorama.setVisible(true);
});
 
    </script>
    <!--<script type="text/javascript" src="pano.js"> </script> -->

</html>
  
