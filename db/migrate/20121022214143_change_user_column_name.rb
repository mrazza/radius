class ChangeUserColumnName < ActiveRecord::Migration
  def change
    change_table :users do |t|
	  t.rename :user_ident, :ident
	  t.rename :user_photo, :photo
	end
  end
end
