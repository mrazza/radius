class AddAuthInfoToUsers < ActiveRecord::Migration
  def change
    add_column :users, :user_ident, :string
    add_column :users, :user_photo, :string
	change_table :users do |t|
	  t.remove :username
	  t.remove :password
	  t.index :user_ident
	end
  end
end
