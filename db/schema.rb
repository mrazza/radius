# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121022214143) do

  create_table "comments", :force => true do |t|
    t.integer  "post_id",     :null => false
    t.integer  "user_id",     :null => false
    t.datetime "time_posted", :null => false
    t.string   "content",     :null => false
  end

  add_index "comments", ["post_id"], :name => "id_idx1"
  add_index "comments", ["user_id"], :name => "id_idx"

  create_table "posts", :force => true do |t|
    t.integer  "user_id",     :null => false
    t.datetime "time_posted", :null => false
    t.string   "content",     :null => false
    t.float    "loc_lat",     :null => false
    t.float    "loc_lon",     :null => false
    t.integer  "radius",      :null => false
    t.integer  "tag_id",      :null => false
  end

  add_index "posts", ["loc_lat"], :name => "LOC_INDEX1"
  add_index "posts", ["loc_lon"], :name => "LOC_INDEX2"
  add_index "posts", ["tag_id"], :name => "id_idx1"
  add_index "posts", ["tag_id"], :name => "tag_idx"
  add_index "posts", ["user_id"], :name => "id_idx"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "tags", :force => true do |t|
    t.string "name", :limit => 45, :null => false
  end

  create_table "users", :force => true do |t|
    t.datetime "date_registered",                :null => false
    t.string   "email",           :limit => 128, :null => false
    t.string   "display_name",    :limit => 45,  :null => false
    t.float    "loc_lat",                        :null => false
    t.float    "loc_lon",                        :null => false
    t.string   "ident"
    t.string   "photo"
  end

  add_index "users", ["email"], :name => "email_UNIQUE", :unique => true
  add_index "users", ["ident"], :name => "index_users_on_user_ident"
  add_index "users", ["loc_lat"], :name => "location_INDEX"
  add_index "users", ["loc_lon"], :name => "location_INDEX2"

end
